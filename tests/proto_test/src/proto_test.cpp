#include "neuro.pb.h"
#include "catch2/catch_test_macros.hpp"
#include "base64_library/base64.h"

using namespace text_process;

TEST_CASE("Example of proto usage", "[proto]")
{
    NeuroRequest nr;
    nr.set_qid(123);
    nr.set_query("ABC");

    CHECK(nr.qid() == 123);
    CHECK(nr.query() == "ABC");
}

TEST_CASE("Encode", "[base64]")
{
    // ARRANGE
    std::string string = "abradimon";
    std::string expected = "YWJyYWRpbW9u";

    // ACT
    std::string result = base64::to_base64(string);

    // ASSERT
    CHECK(expected == result);
}

TEST_CASE("Decode", "[base64]")
{
    // ARRANGE
    std::string string = "YWJyYWRpbW9u";
    std::string expected = "abradimon";

    // ACT
    std::string result = base64::from_base64(string);

    // ASSERT
    CHECK(expected == result);
}
