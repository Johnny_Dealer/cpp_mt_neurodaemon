//
// Created by petr_kuril on 28.01.24.
//

#include "server/server.hpp"


namespace server {
    Server::Server(uint16_t port) {
        // creating socket
        this->_serverSocket = socket(AF_INET, SOCK_STREAM, 0);

        // specifying the address
        this->_socket_address = new sockaddr_in();
        this->_socket_address->sin_family = AF_INET;
        this->_socket_address->sin_port = htons(port);
        this->_socket_address->sin_addr.s_addr = INADDR_ANY;

        std::cerr << "Server constructed" << std::endl;
    }

    void Server::Bind() {
        [[maybe_unused]] auto _ = bind(
                this->_serverSocket,
                reinterpret_cast<const sockaddr *>(this->_socket_address),
                sizeof(struct sockaddr_in)
        );
    }

    void Server::Listen() const {
        listen(this->_serverSocket, 5);
    }

    void Server::Accept() {
        this->_clientSocket = accept(this->_serverSocket, nullptr, nullptr);
    }

    void Server::Receive() {
        Accept();

        // Читаем request (until \n)
        const std::string encodedRequest = ReadStreamedUntilLineBreak();

        // Декодируем
        const std::string decodedRequest = base64::from_base64(encodedRequest);
        std::cout << "[INFO] Received message from client: " << decodedRequest << std::endl;

        // Получаем сообщение из запроса
        NeuroRequest request = GenerateRequest(decodedRequest);
        const std::string& query = request.query();

        // Пихаем сообщение в нейронку и получаем коллекцию float'ов
        std::vector<float> encoded = emb.Encode(query);

        // Создаём response
        NeuroAnswer response = GenerateResponse(request.qid(), encoded);

        // Отправляем response
        std::string serializedResponse = response.SerializeAsString();
        serializedResponse += '\n';
        const std::string response_message = base64::to_base64(serializedResponse);
        send(this->_clientSocket, response_message.c_str(), strlen(response_message.c_str()), 0);
    }


    std::string Server::ReadStreamedUntilLineBreak() const {
        std::stringstream stringStream = std::stringstream();

        char buffer[BUFFER_SIZE] = "";

        long lengthRead = recv(this->_clientSocket, buffer, BUFFER_SIZE, 0);

        stringStream.write(buffer, lengthRead);

        while (buffer[lengthRead - 1] != '\n')
        {
            lengthRead = recv(this->_clientSocket, buffer, BUFFER_SIZE, 0);

            // Если -1, значит возникла ошибка при чтении
            if (lengthRead < 0)
            {
                throw std::runtime_error("Exception raised while receiving data");
            }

            if (lengthRead == 0)
            {
                throw std::runtime_error("Message has not ended up with '\n'");
            }

            stringStream.write(buffer, lengthRead);
        }

        return stringStream.str();
    }


    Server::~Server() {
        close(_serverSocket);
    }

    [[noreturn]] void Server::Serve() {
        Bind();
        Listen();

        std::cout << "Server is now accepting connections..." << std::endl;

        while(true)
        {
            Receive();
        }
    }

    NeuroRequest Server::GenerateRequest(const std::string& data) {
        NeuroRequest neuroRequest = text_process::NeuroRequest();
        neuroRequest.ParseFromString(data);

        return neuroRequest;
    }

    NeuroAnswer Server::GenerateResponse(u_int32_t qid, const std::vector<float>& data) {
        NeuroAnswer neuroResponse = text_process::NeuroAnswer();
        neuroResponse.set_qid(qid);

        if (!data.empty())
        {
            for (float value : data)
                neuroResponse.add_embedding(value);
        }
        else
        {
            neuroResponse.set_error("Embedding error");
        }

        return neuroResponse;
    }

}
