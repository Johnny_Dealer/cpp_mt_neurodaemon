#include "server/server.hpp"

#include <csignal>
#include <cstdlib>
#include <cstdio>

using namespace std;
using namespace server;

#define PORT 8080

void CtrlCHandler(int s) {
    printf("\nCaught Ctrl + C, stopping server. Code: %d\n",s);
    exit(1);
}

void CatchCtrlC() {
    struct sigaction sigIntHandler{};

    sigIntHandler.sa_handler = CtrlCHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, nullptr);
}

int main()
{
    CatchCtrlC();

    auto server = Server(PORT);

    try {
        server.Serve();
    }
    catch (exception& e) {

    }

    return 0;
}
