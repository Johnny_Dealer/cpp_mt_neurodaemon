#!/usr/bin/env python3

import argparse
import base64
import queue
import socket
import threading
import time
from queue import Queue
from typing import Any

HOST = "localhost"
PORT = 8080


def parse_args() -> dict[str, Any]:
    parser = argparse.ArgumentParser(
        prog='ClientApp',
        description='What the program does',
        epilog='Text at the bottom of help'
    )

    parser.add_argument('-f', '--filename', type=str, default='urls.txt', required=True)
    parser.add_argument('-t', '--timeout', type=int, default=10, required=True)

    args = parser.parse_args()
    return vars(args)


def get_line_generator(file_name: str) -> str:
    with open(file_name, 'r', encoding='utf-8') as file:
        while True:
            line = file.readline()
            if not line:
                break
            yield line.strip()


class WorkerThread(threading.Thread):
    def __init__(self, filename: str, timeout: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filename = filename
        self.timeout = timeout
        self.message_queue = queue.Queue()
        self.response_time_list: list[float] = []

    def run(self):
        line_generator = get_line_generator(self.filename)
        for message in line_generator:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                    sock.connect((HOST, PORT))
                    sock.settimeout(self.timeout)

                    start_time = time.time()
                    sock.sendall(base64.b64encode(message.encode()))
                    response = base64.b64decode(sock.recv(1024))
                    end_time = time.time()

                    elapsed_time: float = end_time - start_time
                    self.response_time_list.append(elapsed_time)

                    self.message_queue.put(f"{message}: {response}")
            except Exception as e:
                print(f"Exception while running client worker {self.name}: {e.__str__()}")
            finally:
                print(f"Processed message \'{message}\' with response: {response}")

    def __del__(self):
        print(f"RPS: {len(self.response_time_list) / sum(self.response_time_list)}")
        print(f" \
            MAX: {max(self.response_time_list)}, \
            MIN: {min(self.response_time_list)}, \
            AVG: {sum(self.response_time_list) / len(self.response_time_list)} \
        ")

    def get_queue(self) -> Queue:
        return self.message_queue


def create_client_threads(threads_number: int, filename: str, timeout: int) -> list[WorkerThread]:
    client_threads: list[WorkerThread] = [WorkerThread(filename, timeout) for _ in range(threads_number)]

    return client_threads


def run_client_threads(client_threads: list[WorkerThread]):
    for client_thread in client_threads:
        client_thread.start()
        print(f'Thread {client_thread} started')

    for client_thread in client_threads:
        client_thread.join()
        print(f'Thread {client_thread} stopped')


def main():
    # Начинаем с парсинга аргументов
    args: dict[str, Any] = parse_args()

    threads_number: int = 1
    filename: str = args['filename']  # 'protobuf.txt'
    timeout: int = args['timeout']  # 10

    # Создаём потоки
    client_threads = create_client_threads(threads_number, filename, timeout)

    # Запускаем и ожидаем потоки
    try:
        run_client_threads(client_threads)
    except Exception as e:
        print('An error')


if __name__ == "__main__":
    main()
