//
// Created by petr_kuril on 28.01.24.
//

#pragma once

/* Base64 кодирование */
#include "base64_library/base64.h"

/* Ввод-вывод */
#include <iostream>
#include <cstring>
#include <utility>

/* Сеть */
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

/* protobuf */
#include "neuro.pb.h"
#include "text_process/neuro_predict.hpp"

/* neuro */
using namespace text_process;

#define BUFFER_SIZE 1024

namespace server
{
    struct ServerException : std::exception {
    public:
        explicit ServerException(std::string  message) : _message {std::move( message )}
        {

        }

        [[nodiscard]] const char* what() const noexcept override
        {
            return _message.c_str();
        }

    private:
        const std::string _message;
    };

    class Server {
    public:
        explicit Server(uint16_t port);
        ~Server();

        [[noreturn]] void Serve();

        /**
         * @brief Bind socket
         * */
        void Bind();

        /**
         * @brief listening to the assigned socket
         * */
        void Listen() const;

        /**
         * @brief Accepting connection request
         * */
        void Accept();

        /**
         * @brief Receiving data
         * */
        void Receive();
    private:
        TextEmbedder emb = text_process::TextEmbedder(
                "/home/petr_kuril/Projects/CPlusPlus/neurodaemon/data/e5/sentencepiece.bpe.model",
                "/home/petr_kuril/Projects/CPlusPlus/neurodaemon/data/e5/traced_e5.pt"
        );

        int _serverSocket = 0;
        int _clientSocket = 0;
        sockaddr_in* _socket_address;

        /**
         * @brief Читает stream Protobuf (until '\n')
         * */
        [[nodiscard]] std::string ReadStreamedUntilLineBreak() const;

        /**
         * @brief Генерирует запрос из байтов
         * */
        static NeuroRequest GenerateRequest(const std::string& data);

        /**
         * @brief Создаёт response на основе переденных данных
         * */
        static NeuroAnswer GenerateResponse(u_int32_t qid, const std::vector<float>& data);
    };
}
